import re

def connected_users(ssh):
    res = ssh.execute_command("netstat -tnpa | grep 'ESTABLISHED.*sshd'", sudo=True)
    data = [list(re.sub(' +', ' ',x).split(" ")) for x in res['out']]
    pos = 1

    connections = []
    for conn in data:
        connections += [{'Protocol': conn[0],
                        'Local_Address': conn[3],
                        'Foreign_Address': conn[4],
                        'PID': conn[6],
                        'User': conn[7],
                        'Pos': pos}]
        pos += 1

    return connections

def connection_attempts(ssh):
    res = ssh.execute_command("grep sshd.\*Failed /var/log/auth.log && grep sshd.\*Accepted /var/log/auth.log", sudo=True)

    attempt_list = []
    for attempt in res['out']:
        if 'COMMAND=' in attempt:
            continue
        attempt = list(attempt.split(' '))
        attempt_list += [{'Month': attempt[0],
                          'Date': attempt[1],
                          'Time': attempt[2],
                          'Host': attempt[3],
                          'Status': attempt[5],
                          'Method': attempt[6],
                          'User': attempt[8],
                          'Address': attempt[10],
                          'Port': attempt[12]
                          }]

    return attempt_list

def command_history(ssh, user, shell='bash'):
    file_path = '/home/' + user
    if shell == 'zsh':
        # TODO handle the zsh history weird return value
        file_path += '/.zsh_history'
    else:
        file_path += '/.bash_history'
    
    command = 'cat ' + file_path
    res = ssh.execute_command(command, sudo=True)
    out = res['out']
    return [cmd.strip() for cmd in out]

def sudo_commands(ssh):
    res = ssh.execute_command('grep COMMAND= /var/log/auth.log', sudo=True)
    out = res['out']

    commands = []
    for entry in out:
        sudo_dict = {}
        entry = re.sub(' +', ' ', entry).split(' : ', 1)
        sudo_info = entry[1].split(' ; ', 3)
        command_info = entry[0].split(' ')

        sudo_dict['RUNNING_USER'] = entry[0].split(': ')[1]
        sudo_dict['MONTH'] = command_info[0]
        sudo_dict['DATE'] = command_info[1]
        sudo_dict['TIME'] = command_info[2]

        for field in sudo_info:
            if '=' not in field:
                continue
            field = field.split('=', 1)
            sudo_dict[field[0]] = field[1]

        commands += [sudo_dict]
 
    return commands