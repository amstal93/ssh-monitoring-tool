from flask import Flask, render_template, request, Blueprint, session, redirect, url_for
from functools import wraps
import logging
import secrets
from ssh_client import SSH
import monitor

app = Flask(__name__)
app.config['SECRET_KEY'] = secrets.token_hex(16)
logging.basicConfig(level=logging.DEBUG)
ssh = SSH()


def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login'))
    return wrap

@app.route("/")
@login_required
def index():
    connections = monitor.connected_users(ssh)
    return render_template('index.html', connections=connections)


@app.route("/login", methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        address, port, username, password = (request.form['address'], request.form['port'], request.form['username'], request.form['password'])
        try:
            ssh.connect_server(server_address=address, server_port=int(port), server_username=username, server_pass=password)
            session['logged_in'] = True
            session['username'] = username
            return redirect(url_for('index'))
        except Exception as e:
            error = 'Error: ' + e.__str__()
    return render_template('login.html', error=error)


@app.route('/logout')
@login_required
def logout():
    session.pop('logged_in', None)
    ssh.close()
    return redirect(url_for('login'))

@app.route('/connections')
@login_required
def connections():
    connections = monitor.connection_attempts(ssh)
    return render_template('connections.html', connections=connections)

@app.route('/sudo')
@login_required
def sudo_commands():
    commands = monitor.sudo_commands(ssh)
    return render_template('sudo.html', commands=commands)

@app.route('/<user>')
def user_commands(user):  # Currently only bash commands
    commands = monitor.command_history(ssh, user, shell='bash')
    return render_template('commands.html', commands=commands)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
